#!/usr/bin/env bash

if [ "$#" -ne 2 ]; then
    echo "Usage:  ./fill_sms_tables.sh SQL_DIR THING_NAME"
    echo
    echo "Insert SQL records located in SQL_DIR into empty local SMS tables."
    echo "Datastream linking only works if frontend data has already been populated"
    echo "as it needs an existing database schema to link to."
    echo "The parent directory of this script is assumed to be the TSM_DIR."
    echo 'Set $TSM_DIR to an absolute path to change that.'
    echo
    echo 'EXAMPLE'
    echo './fill_sms_tables.sh sms_data DemoThing'
    exit 1
fi

SQL_DIR=$(basename "$1")
THING_NAME="$2"

if [ -z ${TSM_DIR+x} ]; then
  TSM_DIR=$(realpath "..")
fi
COMPOSE_FILE="${TSM_DIR%/}/tsm-orchestration/docker-compose.yml"
ENV_FILE="${TSM_DIR%/}/tsm-orchestration/.env"

dump=$(mktemp)
creds=$(mktemp)

docker compose -f "$COMPOSE_FILE" exec frontend python3 manage.py dumpdata > "$dump"
python3 > "$creds" << EOF || exit 1
import json
file = "$dump"
thing_name = "$THING_NAME"
with open(file) as f:
    data = json.load(f)
things = {e["fields"]["name"]: e["pk"] for e in data if e.get("model") == "tsm.thing"}
dbs = {e["pk"]: e for e in data if e.get("model") == "tsm.database"}
if thing_name not in things.keys():
    raise LookupError(f"No thing with name {thing_name}")
db = dbs[things[thing_name]]["fields"]
print(f'DB_SCHEMA={db["username"]}')
EOF

grep '^CREATEDB_POSTGRES_*' "$ENV_FILE" >> "$creds"  || exit 1

echo "SQL_DIR=$SQL_DIR"
source "$creds" || exit 1

PSQL_COMMAND="\
PGPASSWORD=$CREATEDB_POSTGRES_PASSWORD \
psql -d $CREATEDB_POSTGRES_DATABASE \
-U $CREATEDB_POSTGRES_USER \
-h localhost"

echo "Upserting sms mock tables"
echo ">> $PSQL_COMMAND -f ${SQL_DIR}/configuration.sql"
eval "$PSQL_COMMAND -f ${SQL_DIR}/configuration.sql"
echo ">> $PSQL_COMMAND -f ${SQL_DIR}/contact.sql"
eval "$PSQL_COMMAND -f ${SQL_DIR}/contact.sql"
echo ">> $PSQL_COMMAND -f ${SQL_DIR}/configuration_contact_role.sql"
eval "$PSQL_COMMAND -f ${SQL_DIR}/configuration_contact_role.sql"
echo ">> $PSQL_COMMAND -f ${SQL_DIR}/configuration_static_location_begin_action.sql"
eval "$PSQL_COMMAND -f ${SQL_DIR}/configuration_static_location_begin_action.sql"
echo ">> $PSQL_COMMAND -f ${SQL_DIR}/device.sql"
eval "$PSQL_COMMAND -f ${SQL_DIR}/device.sql"
echo ">> $PSQL_COMMAND -f ${SQL_DIR}/device_mount_action.sql"
eval "$PSQL_COMMAND -f ${SQL_DIR}/device_mount_action.sql"
echo ">> $PSQL_COMMAND -f ${SQL_DIR}/device_property.sql"
eval "$PSQL_COMMAND -f ${SQL_DIR}/device_property.sql"
echo ">> $PSQL_COMMAND -v datasource=${DB_SCHEMA} -f ${SQL_DIR}/datastream_link.sql"
eval "$PSQL_COMMAND -v datasource=${DB_SCHEMA} -f ${SQL_DIR}/datastream_link.sql"
echo ">> $PSQL_COMMAND -f ${SQL_DIR}/measured_quantity.sql"
eval "$PSQL_COMMAND -f ${SQL_DIR}/measured_quantity.sql"