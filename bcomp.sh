# source this file to enable bash completion for tsm-workflow.sh

function _tsmWF_complete_()
{
    # the command to complete
    # remove everything until last `/`
#    local cmd="${1##*/}"

    # the word the user just entered and we are about to complete
    local cur=${COMP_WORDS[COMP_CWORD]}
    local last=${COMP_WORDS[COMP_CWORD-1]}
    local names=""

    if [ "$last" == "rebuild" ]; then
      if [ -f "$DC_FILE" ]; then
        local opts=(-f "$DC_FILE")
        if [ -f "$DC_DEV_FILE" ]; then
          opts+=(-f "$DC_DEV_FILE")
        fi
        names=$(docker compose "${opts[@]}" ps --services 2>/dev/null)
      fi
    else
      names='clone down clear upfast up rebuild populate upload sms wait all restart'
    fi

    COMPREPLY=($(compgen -W "$names" -- "${cur}"))
}


if [ -z ${TSM_DIR+x} ]; then
  if [ -d "../tsm-orchestration" ]; then
    TSM_DIR=$(realpath "..")
  elif [ -d "./tsm-orchestration" ]; then
    TSM_DIR=$(realpath ".")
  else
    # shellcheck disable=SC2016
    echo 'Please set variable $TSM_DIR and source this file again.'
  fi
fi
DC_FILE="$TSM_DIR/tsm-orchestration/docker-compose.yml"
DC_DEV_FILE="$TSM_DIR/tsm-orchestration/docker-compose-dev.yml"
complete -F _tsmWF_complete_ ./tsm-workflow.sh

export __tsmWF_completion_active=true
