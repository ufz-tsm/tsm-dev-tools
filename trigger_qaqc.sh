#!/usr/bin/env bash

if [ "$#" -ne 1 ]; then
    echo "Usage:  ./trigger_qaqc.sh THING_NAME"
    echo
    echo "The parent directory of where this script lies, is assumed to be the TSM_DIR."
    echo 'Set $TSM_DIR to an absolute path to change that.'
    echo
    echo 'EXAMPLE'
    echo './trigger_qaqc.sh DemoThing'
    exit 1
fi

THING_NAME="$1"

if [ -z ${TSM_DIR+x} ]; then
  TSM_DIR=$(realpath "..")
fi
COMPOSE_FILE="${TSM_DIR%/}/tsm-orchestration/docker-compose.yml"

dump=$(mktemp)
creds=$(mktemp)
docker compose -f "$COMPOSE_FILE" exec -T frontend python3 manage.py dumpdata > "$dump"  || exit 1
python3 > "$creds" <<EOF  || exit 1
import json
import sys
file = "$dump"
thing_name = "$THING_NAME"
with open(file) as f:
    data = json.load(f)
things = {e["fields"]["name"]: e for e in data if e.get("model") == "tsm.thing"}
raws = {e["pk"]: e for e in data if e.get("model") == "tsm.database"}
if thing_name not in things.keys():
    raise LookupError(f"No thing with name {thing_name}")
raw = raws[things[thing_name]["pk"]]["fields"]
print(f'UUID={things[thing_name]["fields"]["thing_id"]}')
print(f'TARGET=postgresql://{raw["username"]}:{raw["password"]}@{raw["url"]}/{raw["name"]}')
EOF


cat "$creds"
source "$creds"  || exit 1

PAYLOAD="{\"thing_uuid\": \"$UUID\", \"db_uri\": \"$TARGET\"}"
#echo $PAYLOAD
mosquitto_pub -L mqtt://mqtt:mqtt@localhost:1883/data_parsed -m "$PAYLOAD"
