
if [ "$#" -eq 0 ]; then
    echo "Usage:  ./tsm-workflow.sh <COMMANDS>"
    echo "Helper script for tsm development."
    echo
    echo "The parent(!) directory of where this script lies, is assumed to be the TSM_DIR."
    echo 'Set $TSM_DIR to an absolute path to change that.'
    echo "Commands that use 'docker', uses the default 'docker-compose.yml' and "
    echo "the 'docker-compose-dev.example.yml' from tsm-orchestration."
    echo
    echo "COMMANDS"
    echo "  clone            clone/pull all TSM repositories, s.a. 'pull_all_tsm_subdirs.sh'"
    echo "  pull             deprecated (use clone)"
    echo "  down             run 'docker compose down --remove-orphans --timeout 0'"
    echo "  clear            run 'remove-all-data.sh' in tsm-orchestration"
    echo "  upfast           run 'docker compose up'"
    echo "  up               run 'docker compose up --build --pull always'"
    echo "  rebuild=SERVICE  run 'docker compose up --build --pull always'"
    echo "  populate         populate frontend database with Demo-{Group,User,Parser,Thing}"
    echo "  upload           upload a DemoFile.csv to object-storage (minio)"
    echo "  sms              fill sms tables with data from sms_data"
    echo "  wait             sleep for 5 seconds "
    echo "  all              alias for 'down clear up populate upload' "
    echo "  restart          alias for 'down upfast' "
    echo
    echo "EXAMPLES"
    echo " ./tsm-workflow pull down up "
    echo
    exit 1
fi

if [ -z ${TSM_DIR+x} ]; then
  TSM_DIR=$(realpath "..")
fi
# rm slash
TSM_DIR="${TSM_DIR%/}"
ORCHEST="$TSM_DIR/tsm-orchestration"
TOOLS="$TSM_DIR/tsm-dev-tools"

COMPOSE_FILE="$ORCHEST/docker-compose.yml"
COMPOSE_EXTRA_ARGS=""
if [ -f "$ORCHEST/docker-compose-dev.yml" ]; then
  COMPOSE_EXTRA_ARGS="-f $ORCHEST/docker-compose-dev.yml"
fi
echo "TSM_DIR=$TSM_DIR"

CMDS="$*"
if [ "$CMDS" == "all" ]; then
  CMDS="down clear up wait populate wait upload sms"
fi
if [ "$CMDS" == "restart" ]; then
  CMDS="down upfast"
fi

LAST_CMD=""
for CMD in $CMDS; do

  if [ "$CMD" == "rebuild" ]; then
    LAST_CMD=$CMD
    continue
  fi

  if [ "$LAST_CMD" == "rebuild" ]; then
    ARG=$CMD
    CMD=$LAST_CMD
    LAST_CMD="unset"
  fi

  echo "running command '$CMD'..."
  case $CMD in

    pull)
      echo "command 'pull' is DEPRECATED use 'clone' instead"
      exit 1
    ;;
    clone)
      echo ">>> bash $TSM_DIR/tsm-dev-tools/pull_all_tsm_subdirs.sh $TSM_DIR"
      bash "$TSM_DIR"/tsm-dev-tools/pull_all_tsm_subdirs.sh "$TSM_DIR"  || exit 1
    ;;

    upfast)
      echo ">>> docker compose -f $COMPOSE_FILE $COMPOSE_EXTRA_ARGS up --detach"
      docker compose -f "$COMPOSE_FILE" $COMPOSE_EXTRA_ARGS up --detach   || exit 1
    ;;

    up)
      echo ">>> docker compose -f $COMPOSE_FILE $COMPOSE_EXTRA_ARGS up --detach --build --pull always"
      docker compose -f "$COMPOSE_FILE" $COMPOSE_EXTRA_ARGS up --detach --build --pull always  || exit 1
    ;;

    down)
      echo ">>> docker compose -f $COMPOSE_FILE $COMPOSE_EXTRA_ARGS down --timeout 0 --remove-orphans"
      docker compose -f "$COMPOSE_FILE" $COMPOSE_EXTRA_ARGS down --timeout 0 --remove-orphans  || exit 1
    ;;

    rebuild)
      echo ">>> docker compose -f $COMPOSE_FILE $COMPOSE_EXTRA_ARGS up --detach --no-deps --build --pull always $ARG"
      docker compose -f "$COMPOSE_FILE" $COMPOSE_EXTRA_ARGS up --detach --no-deps --build --pull always "$ARG" || exit 1
    ;;

    populate)
      for OBJ in "00_user" "01_parser" "02_mqtt_device_types" "10_things" "11_dwd_api"; do
        echo "$CMD: install $OBJ"
        fail_msg="$CMD: install $OBJ failed, trying again.."
        FILE="$TOOLS/frontend_data/$OBJ.json"
        command="bash $TOOLS/populate_frontend.sh $ORCHEST $FILE 2> /dev/null"
        echo ">>> $command"
        timeout 10s bash -c "until $command; do echo $fail_msg; sleep 2; done" || {
            echo "$CMD: FAILED"
            exit 1
          }
      done
      ;;

    upload)
      echo ">>> bash $TOOLS/upload_minio.sh $TOOLS/DemoFile.csv DemoThing"
      bash $TOOLS/upload_minio.sh $TOOLS/DemoFile.csv DemoThing || exit 1
      ;;

    sms)
      bash $TOOLS/fill_sms_tables.sh $TOOLS/sms_data DemoThing || exit 1
      ;;

    clear)
      pushd $ORCHEST > /dev/null || exit 1
      echo ">>> bash remove-all-data.sh"
      bash remove-all-data.sh  || exit 1
      popd > /dev/null  || exit 1
      ;;

    wait)
      for i in 3 2 1; do
        echo "sleep $i" && sleep 1
      done
      ;;

    *)
    echo "$CMD: unknown command"
    exit 3
    ;;

  esac
  echo "command '$CMD' succeeded"
  echo
done

if [ -z $__tsmWF_completion_active ]; then
  echo
  echo "Hint: want bash autocompletion? run \`source bcomp.sh\` and run this by \`./tsm-workflow.sh\`"
fi
