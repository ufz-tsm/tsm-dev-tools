#!/usr/bin/env python3
import json
import sys

if len(sys.argv) != 3:
    print(
        "Usage: ./extract_db_credentials.py FILE THING_NAME\n"
        "Extract the db, user and password \n"
        "for a THING_NAME from a frontend json dump.\n"
        "(use './dump_frontend.sh > FILE') for that.\n"
    )
    sys.exit(1)

file = sys.argv[1]
thing_name = sys.argv[2]
with open(file) as f:
    data = json.load(f)

things = {e["fields"]["name"]: e for e in data if e.get("model") == "tsm.thing"}
if thing_name not in things.keys():
    raise LookupError(f"No thing with name {thing_name}")
thing = things[thing_name]["fields"]
pk = things[thing_name]["pk"]

raws = {e["pk"]: e for e in data if e.get("model") == "tsm.rawdatastorage"}
raw = raws[pk]["fields"]

dbs = {e["pk"]: e for e in data if e.get("model") == "tsm.database"}
db = dbs[pk]["fields"]

print(f'THING_UUID={thing["thing_id"]}')
print(f'BUCKET={raw["bucket"]}')
print(f'BUCKET_USER={raw["access_key"]}')
print(f'BUCKET_PASS={raw["secret_key"]}')
print(f'BUCKET_URI={raw["fileserver_uri"]}')
print(f'DB_URL={db["url"]}')
print(f'DB_NAME={db["name"]}')
print(f'DB_USER={db["username"]}')
print(f'DB_PASS={db["password"]}')
print(f'TARGET=postgresql://{db["username"]}:{db["password"]}@{db["url"]}/{db["name"]}')
