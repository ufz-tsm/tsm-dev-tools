#!/usr/bin/env bash


if [ "$#" -ne 0 ]; then
    echo "Usage:  ./dump_frontend.sh"
    echo "Dump the data from the frontend as json."
    echo
    echo "The parent directory of where this script lies, is assumed to be the TSM_DIR."
    echo 'Set $TSM_DIR to an absolute path to change that.'
    echo
    echo "Examples:"
    echo "  ./populate_frontend.sh > x.json"
    echo
    exit 1
fi

if [ -z ${TSM_DIR+x} ]; then
  TSM_DIR=$(realpath "..")
fi
COMPOSE_FILE="${TSM_DIR%/}/tsm-orchestration/docker-compose.yml"

docker compose -f "$COMPOSE_FILE" exec -T frontend python3 manage.py dumpdata
