#!/usr/bin/env bash


if [ "$#" -ne 2 ]; then
    echo "Usage:  ./populate_frontend.sh DIRECTORY FILE"
    echo "Populate the frontend with data from a json FILE."
    echo
    echo "FILE must be a json file that hold data to populate the frontend."
    echo "DIRECTORY must be the tsm-orchestration directory."
    echo
    echo "Examples:"
    echo "  ./populate_frontend.sh ../tsm-orchestration frontend_data/DemoUser.json"
    echo
    exit 1
fi

COMPOSE_FILE="$(realpath "$1")/docker-compose.yml"
DATAFILE=$(realpath "$2")

docker compose -f "$COMPOSE_FILE" exec -T frontend python3 manage.py loaddata --format json - < "$DATAFILE"

