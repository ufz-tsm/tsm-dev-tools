#!/bin/bash

TARGET_DIR="../tsm-frontend" # tsm-frontend directory based on the "expected" structure provided in the dev-tools-docs

# Check if a tsm-frontend directory argument is provided
if [ "$1" ]; then
    TARGET_DIR=$1
fi

# Check if the provided directory exists
if [ ! -d "$TARGET_DIR" ]; then
    echo "${red}EXITING. Directory '$TARGET_DIR' does not exist.${normal}"
    exit 1
fi

# Check if the passed directory is the tsm-frontend by checking if 'tsm' folder exists
if [ ! -d "$TARGET_DIR/tsm" ]; then
    echo "${red}EXITING. Folder 'tsm' does not exist in '$TARGET_DIR'. Please check if the provided directory is the tsm-frontend.${normal}"
    exit 1
fi

# Function to update JavaScript file with the new model key name
update_js_file() {
    local model_key_name="$1"
    local js_file_path="$TARGET_DIR/tsm/js/qaqc_test_form.js"

    # Insert the new model key immediately after the opening bracket of the array
    sed -i "/const AVAILABLE_FUNCTIONS = \[/ s/\[/[\"$model_key_name\", /" "$js_file_path"

    echo "UPDATED /tsm/js/qaqc_test_form.js with $model_key_name at the beginning of AVAILABLE_FUNCTIONS array"
}

# Function to update Python file with the new function type choice
update_qaqc_test_model() {
    local function_key_name="$1"
    local function_display_name="$2"

    # Add the new choice entry below (None, "Select a function"),
    sed -i "/(None, \"Select a function\"),/a \ \ \ \ (\"$function_key_name\", \"$function_display_name\")," "$TARGET_DIR/tsm/models/QaqcTest.py"

    echo "UPDATED /tsm/models/QaqcTest.py: Added select option: $function_key_name"
}

update_models_init_py() {
    local model_name="$1"

    # Add the import line under the comment '# QaQC functions'
    sed -i "/# QaQC functions/a from tsm.models.qaqc_functions.${model_name} import ${model_name}" "$TARGET_DIR/tsm/models/__init__.py"

    echo "UPDATED /tsm/models/__init__.py: Added import for $model_name"
}

update_admin_classes_init_py() {
    local model_name="$1"

    # Add the import line at the end of the __init__.py file
    echo "from tsm.admin_classes.qaqc_test_function_inlines.${model_name}Inline import ${model_name}Inline" >> "$TARGET_DIR/tsm/admin_classes/__init__.py"

    echo "UPDATED /tsm/admin_classes/__init__.py: Added import for $model_name"
}

update_qaqc_test_inline() {
    local model_name="$1"

    # Add the new inline class at the beginning of the inlines array
    sed -i "/inlines = \[/ s/\[/[${model_name}Inline, /" "$TARGET_DIR/tsm/admin_classes/qaqcTestInline.py"

    sed -i "3i from tsm.admin_classes import ${model_name}Inline" "$TARGET_DIR/tsm/admin_classes/qaqcTestInline.py"

    echo "UPDATED /tsm/admin_classes/qaqcTestInline.py: Added ${model_name}Inline to inlines"
}

convert_property_type() {
    local type="$1"
    case "$type" in
        "int"|"negative int"|"positive int") echo "IntegerField" ;;
        "float") echo "FloatField" ;;
        "bool") echo "BooleanField" ;;
        "string"|"choice") echo "CharField" ;;
        *) echo "UnknownType" ;;
    esac
}

create_model_file() {
    local model_name="$1"
    local function_key_name="$2"
    local properties=("${@:3}")
    local model_file_path="$TARGET_DIR/tsm/models/qaqc_functions/${model_name}.py"

    mkdir -p "$(dirname "$model_file_path")"

    {
        echo "from django.db import models"
        echo ""
        echo "from tsm.models.qaqc_functions.QaqcFunction import QaqcFunction"
        echo ""
        echo "class ${model_name}(QaqcFunction):"
        echo ""
    } > "$model_file_path"

    # Add the properties to the model above the get_function_name method
    for property in "${properties[@]}"; do
        IFS=":" read -r property_name property_display_name type extra required <<< "$property"

        field_type=$(convert_property_type "$type")

        if [[ "$type" == "choice" ]]; then
            choices_list="[(None, \"Select a value\"), "
            IFS=',' read -ra choice_array <<< "$extra"
            for choice in "${choice_array[@]}"; do
                choices_list+="(\"$choice\", \"$choice\"), "
            done
            choices_list="${choices_list%, }]"

            if [[ "$required" == "True" ]]; then
                echo "    $property_name = models.$field_type(\"$property_display_name\", choices=$choices_list)" >> "$model_file_path"
            else
                echo "    $property_name = models.$field_type(\"$property_display_name\", choices=$choices_list, blank=True, null=True)" >> "$model_file_path"
            fi

        elif [[ "$type" == "string" ]]; then
            if [[ "$required" == "True" ]]; then
                echo "    $property_name = models.$field_type(\"$property_display_name\", max_length=400)" >> "$model_file_path"
            else
                echo "    $property_name = models.$field_type(\"$property_display_name\", max_length=400, blank=True, null=True)" >> "$model_file_path"
            fi

        else
            if [[ "$required" == "True" ]]; then
                echo "    $property_name = models.$field_type(\"$property_display_name\")" >> "$model_file_path"
            else
                echo "    $property_name = models.$field_type(\"$property_display_name\", blank=True, null=True)" >> "$model_file_path"
            fi
        fi
    done

    echo -n ""
    echo "    @staticmethod" >> "$model_file_path"
    echo "    def get_function_name():" >> "$model_file_path"
    echo "        return \"$function_key_name\"" >> "$model_file_path"
    echo -n ""
    echo "    def get_properties_json(self):" >> "$model_file_path"
    echo "        return {" >> "$model_file_path"

    # Add each property to the properties JSON
    for property in "${properties[@]}"; do
        IFS=":" read -r property_name property_display_name type extra <<< "$property"
        echo "            \"$property_name\": self.$property_name," >> "$model_file_path"
    done

    echo "        }" >> "$model_file_path"

    echo "CREATED /tsm/models/qaqc_functions/${model_name}.py"
}

create_inline_class_file() {
    local model_name="$1"
    local function_key_name="$2"
    local inline_file_path="$TARGET_DIR/tsm/admin_classes/qaqc_test_function_inlines/${model_name}Inline.py"

    mkdir -p "$(dirname "$inline_file_path")"

    {
        echo "from tsm.admin_classes.qaqcFunctionInline import QaqcFunctionInline"
        echo "from tsm.form_classes.qaqc_function_forms.${model_name}Form import ${model_name}Form"
        echo "from tsm.models.qaqc_functions.${model_name} import ${model_name}"
        echo ""
        echo "class ${model_name}Inline(QaqcFunctionInline):"
        echo "    model = ${model_name}"
        echo "    classes = [\"qaqc-test-function\", \"qaqc-test-function-$function_key_name\"]"
        echo "    form = ${model_name}Form"
    } > "$inline_file_path"

    echo "CREATED /tsm/admin_classes/qaqc_test_function_inlines/${model_name}Inline.py"
}

create_form_class_file() {
    local model_name="$1"
    local properties=("${@:2}")
    local form_file_path="$TARGET_DIR/tsm/form_classes/qaqc_function_forms/${model_name}Form.py"

    mkdir -p "$(dirname "$form_file_path")"

    {
        echo "from django import forms"
        echo ""
        echo "from tsm.form_classes.qaqcFunctionForm import QaqcFunctionForm"
        echo "from tsm.models.qaqc_functions.${model_name} import ${model_name}"
        echo ""
        echo "class ${model_name}Form(QaqcFunctionForm):"

        for property_data in "${properties[@]}"; do
            IFS=":" read -r property_name property_display_name property_type extra required <<< "$property_data"

            case "$property_type" in
                "int"|"negative int"|"positive int")
                    field_type="IntegerField"
                    widget="NumberInput"
                    ;;
                "float")
                    field_type="FloatField"
                    widget="NumberInput"
                    ;;
                "bool")
                    if [[ "$required" == "True" ]]; then
                        field_type="BooleanField"
                        widget=""
                    else
                        field_type="NullBooleanField"
                        widget="NullBooleanSelect"
                    fi
                    ;;
                "string")
                    field_type="CharField"

                    if [[ "$extra" == "textarea" ]]; then
                        widget="Textarea"
                    else
                        widget="TextInput"
                    fi
                    ;;
                *) continue ;;  # Skip unknown types
            esac

            help_text="$property_type"
            [[ "$required" == "False" ]] && help_text+=" | null"

            echo "    $property_name = forms.$field_type("
            if [[ "$widget" == "NumberInput" ]]; then
                # Add min/max attrs based on the type of int
                if [[ "$property_type" == "positive int" ]]; then
                    echo "        widget=forms.$widget(attrs={\"min\":0}),"
                elif [[ "$property_type" == "negative int" ]]; then
                    echo "        widget=forms.$widget(attrs={\"max\":0}),"
                else
                    echo "        widget=forms.$widget(),"
                fi
            elif [[ -n "$widget" ]]; then
                echo "        widget=forms.$widget(),"
            fi
            echo "        label=\"$property_display_name\","
            echo "        required=False,"
            echo "        help_text=\"$help_text\""
            echo "    )"
        done

        echo ""
        echo "    def custom_clean(self, cleaned_data):"

        has_checks=false

        for property_data in "${properties[@]}"; do
            IFS=":" read -r property_name property_display_name property_type extra required <<< "$property_data"
            if [[ "$property_type" == "negative int" ]]; then
                echo "        if \"$property_name\" in cleaned_data and (not float(cleaned_data[\"$property_name\"]) or cleaned_data[\"$property_name\"] >= 0):"
                echo "            self.add_error(\"$property_name\", \"Value must not be positive.\")"
                has_checks=true
            elif [[ "$property_type" == "positive int" ]]; then
                echo "        if \"$property_name\" in cleaned_data and (not float(cleaned_data[\"$property_name\"]) or cleaned_data[\"$property_name\"] <= 0):"
                echo "            self.add_error(\"$property_name\", \"Value must not be negative.\")"
                has_checks=true
            fi
        done

        # If no checks were added, include a pass statement
        if ! $has_checks; then
            echo "        pass"
        fi

        echo ""
        echo "    class Meta:"
        echo "        model = ${model_name}"
        echo "        fields = \"__all__\""
    } > "$form_file_path"

    echo "CREATED /tsm/form_classes/qaqc_function_forms/${model_name}Form.py"
}

bold=$(tput bold)
normal=$(tput sgr0)
red=$(tput setaf 1)


# Model creation begin
echo "${bold}Enter the model name for a new QaQC function${normal}"
echo "Use PascalCase (e. g. 'FlagRangeFunction') and ensure the models does not already exist"
read model_name

echo ""
echo "${bold}Enter the key of the QaQC function '$model_name'${normal}"
echo "Use lowercase letters (e. g. 'flagrange') and ensure the key does not already exist"
read function_key_name

echo ""
echo "${bold}Enter the display name for the QaQC function '$model_name'${normal}"
read function_display_name

# Array to hold properties
declare -a properties
declare -a required_properties

while true; do
    echo ""
    echo "${bold}Enter a new property name or type 'done' to finish adding properties${normal}"
    echo "Use lowercase letters and no reserved properties"
    read property_name

    if [[ "$property_name" == "done" ]]; then
        if [ ${#properties[@]} -eq 0 ]; then
            echo "${red}The model must have at least one property.${normal}"
            continue
        fi
        break
    fi

    if [[ "$property_name" == "test" ||  "$property_name" == "field" ||  "$property_name" == "target" ]]; then
      echo "${red}Invalid property name.${normal}"
      continue
    fi

    echo ""
    echo "${bold}Enter the display name for new property '$property_name'${normal}"
    read property_display_name

    echo ""
    echo "${bold}Select a type for property '$property_name':${normal}"
    echo "1) int"
    echo "2) float"
    echo "3) bool"
    echo "4) negative int"
    echo "5) positive int"
    echo "6) string"
    echo "7) choice"
    read -p "${bold}Enter the corresponding number for the type: ${normal}" property_type

    case $property_type in
        1) type="int" ;;
        2) type="float" ;;
        3) type="bool" ;;
        4) type="negative int" ;;
        5) type="positive int" ;;
        6)
            type="string"
            while true; do
                echo ""
                echo "${bold}Select how the string will be input:${normal}"
                echo "1) Textarea"
                echo "2) Textfield"
                read -p "${bold}Enter the corresponding number: ${normal}" string_input_type_selection

                case $string_input_type_selection in
                    1) string_input_type="textarea"; break ;;
                    2) string_input_type="textfield"; break ;;
                    *)
                        echo ""
                        echo "${red}Invalid selection. Please enter '1' for Textarea or '2' for Textfield.${normal}"
                        ;;
                esac
            done
            ;;
        7)
            type="choice"
            echo ""
            echo "${bold}Enter the list of choices separated by commas (e. g. 'red,green,blue')${normal}"
            read choices
            ;;
        *)
            echo "${red}Invalid type selected, skipping this property.${normal}"
            continue
            ;;
    esac

    while true; do
        echo ""
        echo -n "${bold}Is this property required? (y/n): ${normal}"
        read is_required
        case $is_required in
            y|Y) required="True"; break ;;
            n|N) required="False"; break ;;
            *)
                echo "${red}Invalid selection. Please enter 'y' for required or 'n' for optional.${normal}"
                ;;
        esac
    done

    # Set help text, adding "| null" for optional fields
    help_text="$property_type"
    [[ "$required" == "False" ]] && help_text+=" | null"  # Append "| null" if optional

    # Add property to the properties array
    if [[ "$type" == "choice" ]]; then
        properties+=("$property_name:$property_display_name:$type:$choices:$required")
    elif [[ "$type" == "string" ]]; then
        properties+=("$property_name:$property_display_name:$type:$string_input_type:$required")
    else
        properties+=("$property_name:$property_display_name:$type::$required")
    fi

    echo ""
    echo "${bold}Property '$property_name' (display name: '$property_display_name') with type '$type' added${normal}"
done

# Create new files
create_model_file "$model_name" "$function_key_name" "${properties[@]}"
create_inline_class_file "$model_name" "$function_key_name"
create_form_class_file "$model_name" "${properties[@]}"

# Update existing files
update_js_file "$function_key_name"
update_qaqc_test_model "$function_key_name" "$function_display_name"
update_models_init_py "$model_name"
update_admin_classes_init_py "$model_name"
update_qaqc_test_inline "$model_name"

# Finalizing

echo ""
read -p "${bold}Do you want to perform formatting with Black? (Y/n): ${normal}" format_choice
if [[ "$format_choice" == "y" || "$format_choice" == "Y" || "$format_choice" == "" ]]; then
    RELATIVE_TARGET_PATH=$(realpath "$TARGET_DIR")
    docker run --rm --volume "$RELATIVE_TARGET_PATH":/src --workdir /src pyfound/black:latest_release black .
    echo "Formatting completed using Black."
fi

echo ""
read -p "${bold}Do you want to generate migrations? (Y/n): ${normal}" migrate_choice
if [[ "$migrate_choice" == "y" || "$migrate_choice" == "Y" || "$migrate_choice" == "" ]]; then
    docker compose -f "$TARGET_DIR/docker-compose.yml" run frontend python3 manage.py makemigrations
else
    echo "Skipping generation of migrations file."
fi

docker compose -f "$TARGET_DIR/docker-compose.yml" run frontend python3 manage.py collectstatic --no-input

echo ""
echo "${bold}Creation of QaQC functions finished."
if [[ "$migrate_choice" == "y" || "$migrate_choice" == "Y" || "$migrate_choice" == "" ]]; then
    echo "If your compose setup is currently running, restart it with 'docker compose down -v && docker compose up -d'"
    echo "or migrate with 'docker compose exec frontend python3 manage.py migrate'"
fi
echo "${red}Please validate changes and functionality before commiting anything!${normal}"
