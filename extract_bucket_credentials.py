#!/usr/bin/env python3
import json
import sys

if len(sys.argv) != 3:
    print(
        "Usage: ./extract_bucket_credentials.py FILE THING_NAME\n"
        "Extract the bucket_uri, user and password \n"
        "for a THING_NAME from a frontend json dump.\n"
        "(use './dump_frontend.sh > FILE') for that.\n"
    )
    sys.exit(1)

file = sys.argv[1]
thing_name = sys.argv[2]
with open(file) as f:
    data = json.load(f)

things = {e["fields"]["name"]: e["pk"] for e in data if e.get("model") == "tsm.thing"}
raws = {e["pk"]: e for e in data if e.get("model") == "tsm.rawdatastorage"}
if thing_name not in things.keys():
    raise LookupError(f"No thing with name {thing_name}")
raw = raws[things[thing_name]]["fields"]

print(f'BUCKET={raw["bucket"]}')
print(f'BUCKET_USER={raw["access_key"]}')
print(f'BUCKET_PASS={raw["secret_key"]}')
print(f'BUCKET_URI={raw["fileserver_uri"]}')
print(
    "curl -k "
    '--upload-file "${FILE?must be set and not null}" '
    "--max-time 5 "
    '--user "$BUCKET_USER:$BUCKET_PASS" '
    '"$BUCKET_URI/$BUCKET/$FILE"'
)
