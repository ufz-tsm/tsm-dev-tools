#!/usr/bin/env bash

# do not use this here
#set -e
#set -o pipefail

function cd_or_exit() {
  cd "$1" || exit 1
}

declare -A repos
repos["tsm-dev-tools"]="git@codebase.helmholtz.cloud:ufz-tsm/tsm-dev-tools.git"
repos["tsm-frontend"]="git@codebase.helmholtz.cloud:ufz-tsm/tsm-frontend.git"
repos["tsm-orchestration"]="git@codebase.helmholtz.cloud:ufz-tsm/tsm-orchestration.git"
repos["timeio-db-api"]="git@codebase.helmholtz.cloud:ufz-tsm/timeio-db-api.git"

# ============================================================
# script
# ============================================================

if [ "$#" -ne 1 ]; then
    echo "Please only pass the TSM parent directory"
    exit 1
fi

ENTRY_PWD=$(pwd)
TSM_DIR=$(realpath "$1")
ALL_OK=true

for key in "${!repos[@]}"; do
  cd_or_exit "$TSM_DIR"
  if [ ! -d "$key" ]; then
    read -p "MISSING .. $key does not exist. Should it be cloned (y/n)? " yn
    case $yn in
        Y|y|yes ) git clone ${repos[$key]};;
        *       ) continue;;
    esac
  fi
  cd_or_exit "$key"
  repo=$key

  # is repo dirty / has uncommitted changes
  git diff --quiet
  RESULT=$?

  MSG=""

  DIRTY=$([ $RESULT == 0 ] && echo "clean" || echo "DIRTY")
  BRANCH=$(git branch --show-current)

  if [ $RESULT == 0 ] && ([ "$BRANCH" == "master" ] || [ "$BRANCH" == "main" ]); then
    echo "           Pulling $repo ..."
    if timeout 7s git pull --quiet; then
      MSG="OK ....... $repo is on branch $BRANCH and is $DIRTY"
    else
      MSG="FAILED ... $repo failed to pull"
      ALL_OK=false
    fi

  elif [ "$BRANCH" == "master" ] || [ "$BRANCH" == "main" ]; then
    MSG="DIRTY .... $repo is on branch $BRANCH but is $DIRTY"
    ALL_OK=false
  else
    MSG="FAILED ... $repo is on branch $BRANCH and is $DIRTY"
    ALL_OK=false
  fi
  echo "$MSG"

done

cd_or_exit "$ENTRY_PWD"

echo ""
if $ALL_OK; then
  echo "INFO:   all repository are pulled/up-to-date"
else
  echo "INFO:   some repositories are DIRTY and/or not on branch main/master or pulling failed"
fi
