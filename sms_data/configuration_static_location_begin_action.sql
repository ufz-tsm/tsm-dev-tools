INSERT INTO public.configuration_static_location_begin_action
    (id, x, y, z, label, configuration_id, begin_date, begin_contact_id, begin_description, end_date)
VALUES
    (1, 12.418759, 51.352492, -1.5, 'A public static location action 1', 1, '2014-09-07 00:00:00', 1, 'A public static location action for Demo Configuration 1', '2014-09-07 19:59:00'),
    (2, 12.428759, 51.362492, -2.5, 'A public static location action 2', 1, '2014-09-07 20:00:00', 1, 'A internal static location action for Demo Configuration 2', NULL),
    (3, 12.438759, 51.372492, -3.5, 'A internal static location action 1', 2, '2014-09-07 00:00:00', 1, 'A public static location action for Demo Configuration 1', '2014-09-07 19:59:00'),
    (4, 12.448759, 51.382492, -4.5, 'A internal static location action 2', 2, '2014-09-07 20:00:00', 1, 'A internal static location action for Demo Configuration 2', NULL)
ON CONFLICT (id) DO UPDATE SET
    x = EXCLUDED.x,
    y = EXCLUDED.y,
    z = EXCLUDED.z,
    label = EXCLUDED.label,
    configuration_id = EXCLUDED.configuration_id,
    begin_date = EXCLUDED.begin_date,
    begin_contact_id = EXCLUDED.begin_contact_id,
    begin_description = EXCLUDED.begin_description,
    end_date = EXCLUDED.end_date;