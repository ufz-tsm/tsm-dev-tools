INSERT INTO public.device
    (id, short_name, description, device_type_name, manufacturer_name, model, serial_number, persistent_identifier, is_internal, is_public)
VALUES
    (1, 'DemoDevicePublic', 'This is not a real public device.', 'DemoDeviceTypePub', 'DemoManufacturerPub', 'DemoModelPub', 'DemoDev2024-pub', 'my.123.456.789.pub', false, true),
    (2, 'DemoDeviceInternal', 'This is not a real internal device.', 'DemoDeviceTypeInt', 'DemoManufacturerInt', 'DemoModelInt', 'DemoDev2024-int', 'my.123.456.789.int', true, false)
ON CONFLICT (id) DO UPDATE SET
    short_name = EXCLUDED.short_name,
    description = EXCLUDED.description,
    device_type_name = EXCLUDED.device_type_name,
    manufacturer_name = EXCLUDED.manufacturer_name,
    model = EXCLUDED.model,
    serial_number = EXCLUDED.serial_number,
    persistent_identifier = EXCLUDED.persistent_identifier,
    is_internal = EXCLUDED.is_internal,
    is_public = EXCLUDED.is_public;
