INSERT INTO public.configuration_contact_role
    (role_name, role_uri, id, configuration_id, contact_id)
VALUES
    ('Owner', 'https://roles.com/Owner', 1, 1, 1),
    ('Owner', 'https://roles.com/Owner', 2, 2, 1)
ON CONFLICT (id) DO UPDATE SET
    role_name = EXCLUDED.role_name,
    role_uri = EXCLUDED.role_uri,
    id = EXCLUDED.id,
    configuration_id = EXCLUDED.configuration_id,
    contact_id = EXCLUDED.contact_id;