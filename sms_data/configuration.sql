INSERT INTO public.configuration
    (id, label, description, persistent_identifier, status, project, is_internal, is_public)
VALUES
    (1, 'DemoConfigurationPublic', 'This Configuration is public.', 'my.123.456.788.pub', 'active', 'DemoProject', false, true),
    (2, 'DemoConfigurationInternal', 'This Configuration is internal.', 'my.123.456.789.int', 'active', 'DemoProject', true, false)
ON CONFLICT (id) DO UPDATE SET
    label = EXCLUDED.label,
    description = EXCLUDED.description,
    persistent_identifier = EXCLUDED.persistent_identifier,
    status = EXCLUDED.status,
    project = EXCLUDED.project,
    is_internal = EXCLUDED.is_internal,
    is_public = EXCLUDED.is_public;