INSERT INTO public.contact
    (id, organization, given_name, family_name, email, orcid)
VALUES
    (1, 'DemoOrganization', 'John', 'Wick', 'tsm@hell.wtf', '0000-0001-2345-6789')
ON CONFLICT (id) DO UPDATE SET
    organization = EXCLUDED.organization,
    given_name = EXCLUDED.given_name,
    family_name = EXCLUDED.family_name,
    email = EXCLUDED.email,
    orcid = EXCLUDED.orcid;