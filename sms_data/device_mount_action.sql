INSERT INTO public.device_mount_action
    (id, configuration_id, device_id, offset_x, offset_y, offset_z, begin_date, begin_contact_id, end_date)
VALUES
    (1001, 1, 1, 1.3, 0.3, 0.5, '2024-09-07 00:00:00', 1, NULL),
    (1002, 1, 2, 1.3, 0.3, 0.5, '2024-09-07 00:00:00', 1, NULL),
    (1003, 2, 1, 1.3, 0.3, 0.5, '2024-09-07 00:00:00', 1, NULL),
    (1004, 2, 2, 1.3, 0.3, 0.5, '2024-09-07 00:00:00', 1, NULL)
ON CONFLICT (id) DO UPDATE SET
    configuration_id = EXCLUDED.configuration_id,
    device_id = EXCLUDED.device_id,
    offset_x = EXCLUDED.offset_x,
    offset_y = EXCLUDED.offset_y,
    offset_z = EXCLUDED.offset_z,
    begin_date = EXCLUDED.begin_date,
    begin_contact_id = EXCLUDED.begin_contact_id,
    end_date = EXCLUDED.end_date;
