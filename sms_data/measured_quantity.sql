INSERT INTO public.measured_quantity
    (id, term, provenance_uri, definition)
VALUES
    (1, 'Quantity 1', 'http://example.org/1', 'Definition 1'),
    (2, 'Quantity 2', 'http://example.org/2', 'Definition 2'),
    (3, 'Quantity 3', 'http://example.org/3', 'Definition 3'),
    (4, 'Quantity 4', 'http://example.org/4', 'Definition 4'),
    (5, 'Quantity 5', 'http://example.org/5', 'Definition 5'),
    (6, 'Quantity 6', 'http://example.org/6', 'Definition 6'),
    (7, 'Quantity 7', 'http://example.org/7', 'Definition 7')
ON CONFLICT (id) DO UPDATE SET
    term = EXCLUDED.term,
    provenance_uri = EXCLUDED.provenance_uri,
    definition = EXCLUDED.definition;