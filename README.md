# TSM dev tools

A repository for scripts, helper, tools and stuff.

Most scripts assume you have a directory structure like that:
``` 
TSM_DIRECTORY (can be any dir)
├── tsm-dev-tools
├── tsm-dispatcher
├── tsm-frontend
├── tsm-orchestration
├── timeio-configdb-updater
├── timeio-db-api
└── ... (maybe some more non-tsm directories)
```


## Script `tsm-workflow.sh`
Wrap common task and combine scripts (see sections below) to simple commands.

Run the script without any argument to see its usage.



## Script `pull_all_tsm_subdirs.sh`
Image this script to call `git pull` for each timIO repository.

In detail, the script *tries* to `git checkout` the main (or master) branch of each
tsm subdirectory and pull the latest changes, but stops (before pulling)
if the repository is **dirty**, so it won't destroy any local changes.

> ⚠ The script does **NOT** detect a repo that is clean and on main/master, 
> but **ahead** of the remote origin.

> ⚠ The exit code is always `0`, even if some repos are dirty or not on main/master. 

Run it like so:
```shell
./pull_all_tsm_subdirs.sh DIRECTORY
```
`DIRECTORY` can be an absolute or a relative path. 

For example, from within `tsm-dev-tools` you simply can run 
```sh
``./pull_all_tsm_subdirs.sh ..
```
On run, it generates a summary which repository is dirty, failed to pull or
successfully was cloned.

Run the script without any argument to see its usage.



## Script `./populate_frontend.sh`
Populate the frontend with demo data, because its pain to do it manually every time.
The `frontend` must be running already and not have been populated yet
(e.g. manually), otherwise some data might be overwritten.

Use the files `frontend_data/DemoUser.json` and `frontend_data/DemoThing.json`
for uploading the default demo stuff. 
```shell
./populate_frontend.sh ../tsm-orchestration frontend_data/DemoUser.json
./populate_frontend.sh ../tsm-orchestration frontend_data/DemoThing.json
```
After this is run. Log into object-storage (minio) and upload the file 
`DemoFile.csv` in the new created bucket or simply run the script 
`./upload_minio.sh` which upload a file there programmatically.

Run the script without any argument to see its usage.



## Script `./upload_minio.sh`
Upload any file To the DemoThing bucket on the object storage. 

**Example**
```shell
./upload_minio.sh DemoFile.csv
```
Run the script without any argument to see its usage.



## Script `./create_qaqc_function_in_frontend.sh`
Assists in creating a QaQC function in the tsm-frontend.   
Define new models and properties by answering script prompts.

**Example**
```shell
./create_qaqc_function_in_frontend.sh
```
Run the script without any argument to see its usage.

