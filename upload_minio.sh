#!/usr/bin/env bash

if [ "$#" -ne 2 ]; then
    echo "Usage:  ./upload_minio.sh FILE THING_NAME"
    echo
    echo "Upload FILE to the minio bucket of the thing with THING_NAME."
    echo "FILE is remotely overwritten, if it already exist."
    echo "The parent directory of where this script lies, is assumed to be the TSM_DIR."
    echo 'Set $TSM_DIR to an absolute path to change that.'
    echo
    echo 'EXAMPLE'
    echo './upload_minio.sh DemoFile.csv DemoThing'
    exit 1
fi

FILE=$(basename "$1")
THING_NAME="$2"

if [ -z ${TSM_DIR+x} ]; then
  TSM_DIR=$(realpath "..")
fi
COMPOSE_FILE="${TSM_DIR%/}/tsm-orchestration/docker-compose.yml"

dump=$(mktemp)
creds=$(mktemp)
docker compose -f "$COMPOSE_FILE" exec -T frontend python3 manage.py dumpdata > "$dump"  || exit 1
python3 > "$creds" <<EOF  || exit 1
import json
import sys
file = "$dump"
thing_name = "$THING_NAME"
with open(file) as f:
    data = json.load(f)
things = {e["fields"]["name"]: e["pk"] for e in data if e.get("model") == "tsm.thing"}
raws = {e["pk"]: e for e in data if e.get("model") == "tsm.rawdatastorage"}
if thing_name not in things.keys():
    raise LookupError(f"No thing with name {thing_name}")
raw = raws[things[thing_name]]["fields"]
print(f'BUCKET={raw["bucket"]}')
print(f'BUCKET_USER={raw["access_key"]}')
print(f'BUCKET_PASS={raw["secret_key"]}')
print(f'BUCKET_URI={raw["fileserver_uri"]}')
EOF

echo "FILE=$FILE"
source "$creds"  || exit 1

# curl speaks SFTP =)
# -k, --insecure    skip known_hosts verification
echo ">> curl -k --upload-file $FILE --max-time 5 --user $BUCKET_USER:$BUCKET_PASS $BUCKET_URI/$BUCKET/$FILE"
curl -k --upload-file "$FILE" --max-time 5 --user "$BUCKET_USER:$BUCKET_PASS" "$BUCKET_URI/$BUCKET/$FILE" || exit 1
