SET search_path TO config_db;

do $$
declare confid bigint;
begin
    insert into config_db.qaqc (name, project_id, context_window)
    values ('sta_test_config'::varchar(200), 2, '3d' )
    returning id into confid;

    insert into config_db.qaqc_test (qaqc_id, function, args, position, name, streams)
    values (
            confid,
            'flagRange',
            '{"min": 10, "max": 0, "label": "sta_test"}',
            null,
            'mySTA test function',
            '[{"arg_name": "field", "sta_thing_id": 1, "sta_stream_id": 101, "alias": "T1S101"}]'::jsonb
           );
end $$;


