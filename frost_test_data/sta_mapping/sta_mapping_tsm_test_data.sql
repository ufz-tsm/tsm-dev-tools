--INSERT INTO sta_mapping.thing (id, name, uuid, description, properties)
--VALUES  (1, 'STA_mapping_test_thing', '513504d4-87ce-4793-baf4-8bbc3c91cd66', 'Just a thing to test STA_mapping implementation', '{"parsers": [], "default_parser": "csv_parser"}');

INSERT INTO stamapping_99abaf7448244f379d9e242ea2f4896d.datastream (id, name, description, properties, position, thing_id)
VALUES  (1, 'STA_test/air_temperature', 'Air Temperature', '{"created_at": "2023-01-01 00:00:00.000000"}', 'air_temperature', 1),
        (2, 'STA_test/air_pressure', 'Air Pressure', '{"created_at": "2023-01-01 00:00:00.000000"}', 'air_pressure', 1),
        (3, 'STA_test/longitude', 'X', '{"created_at": "2023-01-01 00:00:00.000000"}', 'longitude', 1),
        (4, 'STA_test/latitude', 'Y', '{"created_at": "2023-01-01 00:00:00.000000"}', 'latitude', 1),
        (5, 'STA_test/height', 'Z', '{"created_at": "2023-01-01 00:00:00.000000"}', 'height', 1);
        
INSERT INTO stamapping_99abaf7448244f379d9e242ea2f4896d.observation (phenomenon_time_start, phenomenon_time_end, result_time, result_type, result_number, result_string, result_json, result_boolean, result_latitude, result_longitude, result_altitude, result_quality, valid_time_start, valid_time_end, parameters, datastream_id)
VALUES  (null, null, '2023-01-01 01:30:00.000000 +01:00', 0, 21, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 02:30:00.000000 +01:00', 0, 22, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 03:30:00.000000 +01:00', 0, 25, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 04:30:00.000000 +01:00', 0, 25, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 05:30:00.000000 +01:00', 0, 25, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 06:30:00.000000 +01:00', 0, 26, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 07:30:00.000000 +01:00', 0, 20, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 08:30:00.000000 +01:00', 0, 15, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 09:30:00.000000 +01:00', 0, 10, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 10:30:00.000000 +01:00', 0, 20, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	
	(null, null, '2023-01-01 11:30:00.000000 +01:00', 0, 21, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 12:30:00.000000 +01:00', 0, 22, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 13:30:00.000000 +01:00', 0, 25, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 14:30:00.000000 +01:00', 0, 25, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 15:30:00.000000 +01:00', 0, 25, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 16:30:00.000000 +01:00', 0, 26, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 17:30:00.000000 +01:00', 0, 20, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 18:30:00.000000 +01:00', 0, 15, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 19:30:00.000000 +01:00', 0, 10, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-01 20:30:00.000000 +01:00', 0, 20, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	
	(null, null, '2023-01-02 01:30:00.000000 +01:00', 0, 21, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 02:30:00.000000 +01:00', 0, 22, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 03:30:00.000000 +01:00', 0, 25, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 04:30:00.000000 +01:00', 0, 25, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 05:30:00.000000 +01:00', 0, 25, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 06:30:00.000000 +01:00', 0, 26, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 07:30:00.000000 +01:00', 0, 20, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 08:30:00.000000 +01:00', 0, 15, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 09:30:00.000000 +01:00', 0, 10, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 10:30:00.000000 +01:00', 0, 20, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	
	(null, null, '2023-01-02 11:30:00.000000 +01:00', 0, 21, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 12:30:00.000000 +01:00', 0, 22, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 13:30:00.000000 +01:00', 0, 25, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 14:30:00.000000 +01:00', 0, 25, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 15:30:00.000000 +01:00', 0, 25, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 16:30:00.000000 +01:00', 0, 26, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 17:30:00.000000 +01:00', 0, 20, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 18:30:00.000000 +01:00', 0, 15, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 19:30:00.000000 +01:00', 0, 10, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),
	(null, null, '2023-01-02 20:30:00.000000 +01:00', 0, 20, null, 'null', null, null, null, null, 'null', null, null, '{}', 1),	
	
	(null, null, '2023-01-01 01:30:00.000000 +01:00', 0, 1100, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 02:30:00.000000 +01:00', 0, 1020, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 03:30:00.000000 +01:00', 0, 1000, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 04:30:00.000000 +01:00', 0, 1300, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 05:30:00.000000 +01:00', 0, 1050, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 06:30:00.000000 +01:00', 0, 1100, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 07:30:00.000000 +01:00', 0, 1001, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 08:30:00.000000 +01:00', 0, 1200, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 09:30:00.000000 +01:00', 0, 1150, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 10:30:00.000000 +01:00', 0, 1000, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	
	(null, null, '2023-01-01 11:30:00.000000 +01:00', 0, 1100, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 12:30:00.000000 +01:00', 0, 1020, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 13:30:00.000000 +01:00', 0, 1000, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 14:30:00.000000 +01:00', 0, 1300, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 15:30:00.000000 +01:00', 0, 1050, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 16:30:00.000000 +01:00', 0, 1100, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 17:30:00.000000 +01:00', 0, 1001, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 18:30:00.000000 +01:00', 0, 1200, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 19:30:00.000000 +01:00', 0, 1150, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-01 20:30:00.000000 +01:00', 0, 1000, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	
	(null, null, '2023-01-02 01:30:00.000000 +01:00', 0, 1100, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 02:30:00.000000 +01:00', 0, 1020, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 03:30:00.000000 +01:00', 0, 1000, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 04:30:00.000000 +01:00', 0, 1300, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 05:30:00.000000 +01:00', 0, 1050, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 06:30:00.000000 +01:00', 0, 1100, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 07:30:00.000000 +01:00', 0, 1001, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 08:30:00.000000 +01:00', 0, 1200, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 09:30:00.000000 +01:00', 0, 1150, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 10:30:00.000000 +01:00', 0, 1000, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	
	(null, null, '2023-01-02 11:30:00.000000 +01:00', 0, 1100, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 12:30:00.000000 +01:00', 0, 1020, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 13:30:00.000000 +01:00', 0, 1000, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 14:30:00.000000 +01:00', 0, 1300, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 15:30:00.000000 +01:00', 0, 1050, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 16:30:00.000000 +01:00', 0, 1100, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 17:30:00.000000 +01:00', 0, 1001, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 18:30:00.000000 +01:00', 0, 1200, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 19:30:00.000000 +01:00', 0, 1150, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	(null, null, '2023-01-02 20:30:00.000000 +01:00', 0, 1000, null, 'null', null, null, null, null, 'null', null, null, '{}', 2),
	
	(null, null, '2023-01-01 01:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-01 02:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-01 03:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-01 04:30:00.000000 +01:00', 0, 0.5, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-01 05:30:00.000000 +01:00', 0, 0.5, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-01 06:30:00.000000 +01:00', 0, 1, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-01 07:30:00.000000 +01:00', 0, 1, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-01 08:30:00.000000 +01:00', 0, 0.5, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-01 09:30:00.000000 +01:00', 0, 0.5, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-01 10:30:00.000000 +01:00', 0, 1, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	
	(null, null, '2023-01-02 01:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-02 02:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-02 03:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-02 04:30:00.000000 +01:00', 0, 0.5, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-02 05:30:00.000000 +01:00', 0, 0.5, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-02 06:30:00.000000 +01:00', 0, 1, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-02 07:30:00.000000 +01:00', 0, 1, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-02 08:30:00.000000 +01:00', 0, 0.5, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-02 09:30:00.000000 +01:00', 0, 0.5, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	(null, null, '2023-01-02 10:30:00.000000 +01:00', 0, 1, null, 'null', null, null, null, null, 'null', null, null, '{}', 3),
	
	(null, null, '2023-01-01 01:30:00.000000 +01:00', 0, 1, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-01 02:30:00.000000 +01:00', 0, 0.5, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-01 03:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-01 04:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-01 05:30:00.000000 +01:00', 0, 0.5, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-01 06:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-01 07:30:00.000000 +01:00', 0, 0.5, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-01 08:30:00.000000 +01:00', 0, 0.5, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-01 09:30:00.000000 +01:00', 0, 1, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-01 10:30:00.000000 +01:00', 0, 1, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	
	(null, null, '2023-01-02 01:30:00.000000 +01:00', 0, 1, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-02 02:30:00.000000 +01:00', 0, 0.5, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-02 03:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-02 04:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-02 05:30:00.000000 +01:00', 0, 0.5, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-02 06:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-02 07:30:00.000000 +01:00', 0, 0.5, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-02 08:30:00.000000 +01:00', 0, 0.5, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-02 09:30:00.000000 +01:00', 0, 1, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),
	(null, null, '2023-01-02 10:30:00.000000 +01:00', 0, 1, null, 'null', null, null, null, null, 'null', null, null, '{}', 4),	
	
	(null, null, '2023-01-01 01:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-01 02:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-01 03:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-01 04:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-01 05:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-01 06:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-01 07:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-01 08:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-01 09:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-01 10:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	
	(null, null, '2023-01-02 01:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-02 02:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-02 03:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-02 04:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-02 05:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-02 06:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-02 07:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-02 08:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-02 09:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5),
	(null, null, '2023-01-02 10:30:00.000000 +01:00', 0, 0, null, 'null', null, null, null, null, 'null', null, null, '{}', 5);
