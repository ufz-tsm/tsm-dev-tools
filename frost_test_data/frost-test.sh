#!/bin/bash

source .env

cat thing-event-msg.json | docker compose -f ../../tsm-orchestration/docker-compose.yml exec -T mqtt-broker sh -c "mosquitto_pub -t thing_creation -u \$MQTT_USER -P \$MQTT_PASSWORD -s"

echo "waiting two seconds for database schema to be set up before injecting tsm/sms-sql into database"
sleep 2

PGPASSWORD=$CREATEDB_POSTGRES_PASSWORD psql -U $CREATEDB_POSTGRES_USER -d $CREATEDB_POSTGRES_DATABASE -h localhost -a -f tsm.sql
PGPASSWORD=$CREATEDB_POSTGRES_PASSWORD psql -U $CREATEDB_POSTGRES_USER -d $CREATEDB_POSTGRES_DATABASE -h localhost -a -f sms.sql

echo "done"
