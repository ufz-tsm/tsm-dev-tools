INSERT INTO public.contact (id,given_name,family_name,website,email,active,created_at,updated_at,created_by_id,updated_by_id) VALUES
	 (1,'User','Test',NULL,'user.test@ufz.de',true,'2023-01-26 13:41:15.094087+01','2023-01-26 13:41:15.0941+01',NULL,NULL);

INSERT INTO public."user" (id,subject,contact_id,active,is_superuser,apikey) VALUES
	 (1,'user@ufz.de',1,true,false,'test_apikey');

INSERT INTO public.device (id,created_at,updated_at,description,short_name,long_name,serial_number,manufacturer_uri,manufacturer_name,dual_use,model,inventory_number,persistent_identifier,website,device_type_uri,device_type_name,status_uri,status_name,created_by_id,updated_by_id,group_ids,is_private,is_internal,is_public,update_description,archived,identifier_type,schema_version) VALUES
	 (3,'2023-01-26 14:26:50.43044+01','2023-01-26 14:28:38.432357+01','','Brightsky Wind Speed','','','','',false,'','',NULL,'','https://localhost.localdomain/cv/api/v1/equipmenttypes/29/','Wind Van','https://localhost.localdomain/cv/api/v1/equipmentstatus/2/','In Use',1,1,'{dpvm-10}',false,true,false,'create;measured quantity',false,'handler','1.0'),
	 (2,'2023-01-26 14:25:42.360325+01','2023-01-26 14:29:29.592616+01','','Brightsky Temperature','','','','',false,'','',NULL,'','https://localhost.localdomain/cv/api/v1/equipmenttypes/26/','Thermometer','https://localhost.localdomain/cv/api/v1/equipmentstatus/2/','In Use',1,1,'{dpvm-10}',false,true,false,'create;measured quantity',false,'handler','1.0');

INSERT INTO public.device_property (id,measuring_range_min,measuring_range_max,failure_value,accuracy,"label",unit_uri,unit_name,compartment_uri,compartment_name,property_uri,property_name,sampling_media_uri,sampling_media_name,device_id,resolution,resolution_unit_name,resolution_unit_uri) VALUES
	 (2,NULL,NULL,NULL,NULL,'Wind Gust Speed 10','https://localhost.localdomain/cv/api/v1/units/56/','m/s','https://localhost.localdomain/cv/api/v1/compartments/1/','Atmosphere','https://localhost.localdomain/cv/api/v1/measuredquantities/344/','Wind gust speed','https://localhost.localdomain/cv/api/v1/samplingmedia/9/','Weather',3,NULL,'',''),
	 (3,-80.0,60.0,NULL,NULL,'Temperature','https://localhost.localdomain/cv/api/v1/units/51/','°C','https://localhost.localdomain/cv/api/v1/compartments/1/','Atmosphere','https://localhost.localdomain/cv/api/v1/measuredquantities/4/','Air temperature','https://localhost.localdomain/cv/api/v1/samplingmedia/9/','Weather',2,NULL,'','');

INSERT INTO public.platform (id,created_at,updated_at,description,short_name,long_name,manufacturer_uri,manufacturer_name,model,platform_type_uri,platform_type_name,status_uri,status_name,website,inventory_number,serial_number,persistent_identifier,created_by_id,updated_by_id,group_ids,is_private,is_internal,is_public,update_description,archived,identifier_type,schema_version) VALUES
	 (2,'2023-01-26 14:30:06.325167+01','2023-01-26 14:30:06.365418+01','','Brightsky Platform','','','','','https://localhost.localdomain/cv/api/v1/platformtypes/1/','Station','https://localhost.localdomain/cv/api/v1/equipmentstatus/2/','In Use','','','',NULL,1,1,'{dpvm-10}',false,true,false,'create;basic data',false,NULL,NULL);

INSERT INTO public."configuration" (id,created_at,updated_at,start_date,end_date,created_by_id,updated_by_id,"label",status,cfg_permission_group,is_internal,is_public,update_description,archived,site_id) VALUES
	 (3,'2023-01-26 14:30:57.819096+01','2023-01-26 14:37:22.47024+01','2023-01-01 13:00:00+01',NULL,1,1,'Brightsky Configuration','active','dpvm-10',true,false,'create;device mount action',false,NULL);

INSERT INTO public.platform_mount_action (id,created_at,updated_at,configuration_id,platform_id,parent_platform_id,begin_date,begin_description,begin_contact_id,offset_x,offset_y,offset_z,created_by_id,updated_by_id,end_date,end_description,end_contact_id) VALUES
	 (2,'2023-01-26 14:36:53.907528+01','2023-01-26 14:36:53.907539+01',3,2,NULL,'2023-01-01 13:41:00+01','',1,0.0,0.0,0.0,1,1,NULL,NULL,NULL);

INSERT INTO public.device_mount_action (id,created_at,updated_at,configuration_id,device_id,parent_platform_id,begin_date,begin_description,begin_contact_id,offset_x,offset_y,offset_z,created_by_id,updated_by_id,end_date,end_description,end_contact_id) VALUES
	 (2,'2023-01-26 14:37:22.279203+01','2023-01-26 14:37:22.279214+01',3,2,2,'2023-01-01 13:41:00+01','',1,0.0,0.0,0.0,NULL,NULL,NULL,NULL,NULL),
	 (3,'2023-01-26 14:37:22.529818+01','2023-01-26 14:37:22.529835+01',3,3,2,'2023-01-01 13:41:00+01','',1,0.0,0.0,0.0,NULL,NULL,NULL,NULL,NULL);

INSERT INTO public.datastream_link (created_at,updated_at,device_property_id,device_mount_action_id,tsm_endpoint,datasource_id,thing_id,datastream_id,begin_date,end_date,created_by_id,updated_by_id) VALUES
	 ('2023-01-26 14:40:06.517579+01','2023-01-26 14:40:06.517595+01',3,2,'foo','1','1','22','2023-01-14 00:00:00+01','2023-01-16 23:59:59+01',NULL,NULL),
	 ('2023-01-26 14:40:15.667728+01','2023-01-26 14:40:15.667742+01',2,3,'foo','1','1','19','2023-01-14 00:00:00+01','2023-01-16 23:59:59+01',NULL,NULL),
	 ('2023-01-26 14:40:06.517579+01','2023-01-26 14:40:06.517595+01',3,2,'foo','1','1','19','2023-01-17 00:00:00+01','2023-01-19 23:59:59+01',NULL,NULL),
	 ('2023-01-26 14:40:15.667728+01','2023-01-26 14:40:15.667742+01',2,3,'foo','1','1','22','2023-01-17 00:00:00+01','2023-01-19 23:59:59+01',NULL,NULL),
	 ('2023-01-26 14:40:06.517579+01','2023-01-26 14:40:06.517595+01',3,2,'foo','1','1','22','2023-01-20 00:00:00+01','2023-01-22 23:59:59+01',NULL,NULL),
	 ('2023-01-26 14:40:15.667728+01','2023-01-26 14:40:15.667742+01',2,3,'foo','1','1','19','2023-01-20 00:00:00+01','2023-01-22 23:59:59+01',NULL,NULL);